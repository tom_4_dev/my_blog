---
layout: post
author: Santanu Bhattacharya | Sanjib Mukherjee
title: Bridges on the river of Ganges
categories: travel

bridges:
- name: Vidyasagar Setu
  time: 1992
  place: Kolkata
  distance: 10
  url: "https://c0.wallpaperflare.com/preview/246/489/85/india-kolkata-vidyasagar-setu-nature.jpg"
- name: Howrah Bridge
  time: 1943
  place: Kolkata
  distance: 10
  url: "https://upload.wikimedia.org/wikipedia/commons/9/93/Howrah_Bridge-Kolkata-West_Bengal.png"
- name: Nivedita Setu
  time: 2007
  place: Dakhineswar
  distance: 10
  url: "https://www.sbp.de/fileadmin/sbp.de/projects/064C812B8C820CB9C1257F2200519E5A_0_1_Nivedita_bridge_2_MAX.jpg"
- name: Vivekananda Setu
  time: 1930
  place: Dakhineswar
  distance: 10
  url: "https://upload.wikimedia.org/wikipedia/commons/9/9c/Hooghly_River1.jpg"
- name: Jubille Bridge
  time: 1885
  place: Chuchura
  distance: 10
  url: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Jubilee_Bridge_%28Naihati-Bandel%29_by_Piyal_Kundu.jpg/250px-Jubilee_Bridge_%28Naihati-Bandel%29_by_Piyal_Kundu.jpg"
- name: Iswar Gupta Setu
  time: 1989
  place: Kalyani
  distance: 10
  url: "https://i.ytimg.com/vi/V_kYGf6e9Mo/maxresdefault.jpg"
- name: Gouranga Setu
  time: 1938
  place: Nabawdeep
  distance: 10
  url: "http://photos.wikimapia.org/p/00/02/03/65/67_big.jpg"
- name: Ramendra Sundar Tribedi Bridge
  time: 1989
  place: Behrempore
  distance: 10
  url: "https://i.ytimg.com/vi/3S3Jg-4PEdE/maxresdefault.jpg"
- name: Nasipur railway bridge
  time: 1872
  place: Nasipur
  distance: 10
  url: "https://st.indiarailinfo.com/kjfdsuiemjvcya24/0/6/2/6/4621626/8/235113662708749767688296620030053778214454o634344.jpg"
- name: Farakka barrage bridge
  time: 1975
  place: Farraka
  distance: 10
  url: "https://images.assettype.com/prothomalo-english/import/media/2018/07/02/2bb53bfc68a528f0bec1dffc68c87ea0-Farakkah-Barrage.jpg"
- name: Vikramshila Setu
  time: 2001
  place: Bhagalpur
  distance: 10
  url: "https://images1.livehindustan.com/uploadimage/library/2020/07/12/16_9/16_9_1/vikramshila_setu_1594524907.jpg"
- name: Rajendra Setu
  time: 1959
  place: Simaria
  distance: 10
  url: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Rajendra_Bridge_at_Simariya.jpg/1200px-Rajendra_Bridge_at_Simariya.jpg"
- name: Mahatma Gandhi Setu
  time: 1982
  place: Patna
  distance: 10
  url: "https://constructionweekonlineindia.storage.googleapis.com/public/images/2020/01/29/Screenshot-%281%29.png"
- name: Digha Sonpur bridge
  time: 2001
  place: Sonpur
  distance: 10
  url: "https://upload.wikimedia.org/wikipedia/commons/5/58/Jppul.png"
- name: Arrah Chhapra Ganga Bridge
  time: 2017
  place: Chhapra
  distance: 10
  url: "https://lh3.googleusercontent.com/proxy/sAx_wCbe7tyCt8RQWyOXu0mlqXAwg4d1-txZmgq7g86JznZaGdZ37AYJcZFxk-HiftT7o1CvAMJhcbPqFpJVN2W6f5mJuaF1kRnld1PtuKq_fmjWKhNN6A"
- name: Beyasi Ballia Bridge
  time: 2019
  place: Ballia
  distance: 10
  url: "https://lh3.googleusercontent.com/proxy/ysMt9B0kdPdIBDYqKhbEj-I0kJeaXsYxLZrglzaMAGIzm4L_VbP2KnT5bvFF1OaNawlszNddLUpwknWt5cDupNw1CwnO9s2MfnmAq9k7UxlR"
- name: Zamania new bridge
  time: 2015
  place: Ghazipur
  url: https://i.ytimg.com/vi/sy5QSA_DcMY/maxresdefault.jpg
- name: Saidpur Ganga bridge
  time: 2015
  place: Saidpur
  url: https://i.ytimg.com/vi/B8ACwVrKX8c/maxresdefault.jpg
- name: Balua ghat bridge
  time: 2017
  place: Varanasi
  url: https://i.ytimg.com/vi/thPITX4uqNQ/hqdefault.jpg
- name: Malavya bridge
  time: 1887
  place: Varanasi
  url: https://i.pinimg.com/originals/6f/f0/5a/6ff05a87858af247d1d35b27982241f7.jpg
- name: VishwaSundari Bridge
  time: 2015
  place: Varanasi
  url: https://i.ytimg.com/vi/1r-M1JNQfsE/maxresdefault.jpg
- name: Lal Bahadur Shastri bridge
  time: xxxx
  place: Mirzapur
  url: https://i.ytimg.com/vi/hywrlkUv4r8/mqdefault.jpg





---
On a lazy weekend evening, I suddenly had a question that how many bridges are on the holy river Ganges. I opened my laptop, and started to traverse through the Ganges with help of Google Earth. And I could not finish the counting on that evening. Then I started to make a list of those bridges in a google sheet and later I was able to convert that sheet in a blog.

Here are the list I am trying to present, in an easiest way...